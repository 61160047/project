/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;


/**
 *
 * @author TAO
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Latte", 40);
        Product p2 = new Product(2,"Choco", 40);
        Employee seller = new Employee("som","0874475587","password");
        Customer customer = new Customer("chai","0548879986");      
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,2);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1,3);
        receipt.addReceiptDetail(p1,3);
        System.out.println(receipt);
    }
}
