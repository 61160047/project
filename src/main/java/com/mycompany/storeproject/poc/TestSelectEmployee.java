/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;


/**
 *
 * @author TAO
 */
public class TestSelectEmployee {
   public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                Employee employee = new Employee(id,name,tel,password);
                System.out.println(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
