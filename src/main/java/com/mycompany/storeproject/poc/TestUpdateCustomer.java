/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TAO
 */
public class TestUpdateCustomer {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Aek karut");
            stmt.setString(2, "0987745513");
            stmt.setInt(3, 1);
            int row = stmt.executeUpdate();          
            System.out.println("Affect row" + row);           
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
