/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestReceipt;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author TAO
 */
public class ReceiptDao implements DaoInterface<Receipt>{

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (cus_id,emp_id,total) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);           
            stmt.setInt(1,object.getCustomer().getId());
            stmt.setInt(2,object.getSeller().getId());
            stmt.setDouble(3,object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
                object.setId(id);
            } 
            for(ReceiptDetail r:object.getReceiptDetail()){
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,pro_id,price,amount) VALUES (?,?,?,?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);           
                stmtDetail.setInt(1,r.getReceipt().getId());
                stmtDetail.setInt(2,r.getProduct().getId());
                stmtDetail.setDouble(3,r.getPrice());
                stmtDetail.setInt(4,r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if(resultDetail.next()){
                    id = resultDetail.getInt(1);
                    r.setId(id);
                } 
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n" +
                        "       created,\n" +
                        "       cus_id,\n" +
                        "       c.name as cus_name,\n" +
                        "       c.tel as cus_tel,\n" +
                        "       emp_id,\n" +
                        "       e.name as emp_name,\n" +
                        "       e.tel as emp_tel,\n" +
                        "       total\n" +
                        " FROM receipt r,customer c,employee e\n" +
                        " WHERE r.cus_id = c.id AND r.emp_id = e.id"+
                        " ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("cus_id");
                String customerName = result.getString("cus_name");
                String customerTel = result.getString("cus_tel");
                int employeeId = result.getInt("emp_id");
                String employeeName = result.getString("emp_name");
                String employeeTel = result.getString("emp_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id,created,
                        new Employee(employeeId,employeeName,employeeTel),
                        new Customer(customerId,customerName,customerTel));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt"+ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt"+ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n" +
                        "       created,\n" +
                        "       cus_id,\n" +
                        "       c.name as cus_name,\n" +
                        "       c.tel as cus_tel,\n" +
                        "       emp_id,\n" +
                        "       e.name as emp_name,\n" +
                        "       e.tel as emp_tel,\n" +
                        "       total\n" +
                        "FROM receipt r,customer c,employee e\n" +
                        "WHERE r.id = ? AND r.cus_id = c.id AND r.emp_id = e.id";
            PreparedStatement stmt = conn.prepareStatement(sql); 
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("cus_id");
                String customerName = result.getString("cus_name");
                String customerTel = result.getString("cus_tel");
                int employeeId = result.getInt("emp_id");
                String employeeName = result.getString("emp_name");
                String employeeTel = result.getString("emp_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(rid,created,
                        new Employee(employeeId,employeeName,employeeTel),
                        new Customer(customerId,customerName,customerTel));
                
                String sqlDetail = "SELECT rd.id as id,\n" +
                                "       receipt_id,\n" +
                                "       pro_id,\n" +
                                "       p.name as pro_name,\n" +
                                "       p.price as pro_price,\n" +
                                "       rd.price as price,\n" +
                                "       amount\n" +
                                " FROM receipt_detail rd, product p\n" +
                                " WHERE receipt_id = ? AND rd.pro_id = p.id;";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail); 
                stmtDetail.setInt(1, id);
                ResultSet resultDetail = stmtDetail.executeQuery();
                while(resultDetail.next()){
                    int receiptId = resultDetail.getInt("id");
                    int productId = resultDetail.getInt("pro_id");
                    String productName = resultDetail.getString("pro_name");
                    double productPrice = resultDetail.getDouble("pro_price");
                    double price = resultDetail.getDouble("price");
                    int amount = resultDetail.getInt("amount");
                    Product product = new Product(productId, productName, productPrice);
                    receipt.addReceiptDetail(receiptId,product, amount,price);
                }
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id "+ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt"+ex.getMessage());
        }
        return null;
    }

    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();          
        } catch (SQLException ex) {
             System.out.println("Error: Unable to delete receipt id "+id+"!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        /*Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row =0;
        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());
            stmt.setInt(3,object.getId());
            row = stmt.executeUpdate();                      
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();*/
        return 0;
    }
    public static void main(String[] args) {
        Product p1 = new Product(1,"Latte", 45);
        Product p2 = new Product(2,"Chocoice", 50);
        Employee seller = new Employee(1,"Supasin Kongkhiaw","0981234567");
        Customer customer = new Customer(1,"Aek karut","0987745513");      
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,2);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println(" id = "+dao.add(receipt));
        System.out.println("Receipt after add: "+receipt);
        System.out.println("Get all: "+dao.getAll());
        
       Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt: "+newReceipt);
    }
}
