/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author TAO
 */
public class EmployeeDao implements DaoInterface<Employee>{

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,tel,password) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getTel());
            stmt.setString(3,object.getPassword());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                Employee employee = new Employee(id,name,tel,password);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {       
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM employee WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                Employee employee = new Employee(pid,name,tel,password);
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }

    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE employee SET name = ?,tel = ?,password = ? WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getTel());
            stmt.setString(3,object.getPassword());
            stmt.setInt(4,object.getId());
            row = stmt.executeUpdate();                               
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(-1,"tao","0987899987","qwe"));
        Employee lastemployee = dao.get(id);
        System.out.println("last employee: "+lastemployee);
        lastemployee.setTel("0328875598");
        System.out.println(dao.get(id));
        int row = dao.update(lastemployee);
        Employee updateemployee = dao.get(id);
        System.out.println("update employee: "+updateemployee);
        dao.Delete(id);
        Employee deleteemployee = dao.get(id);
        System.out.println("delete employee: "+deleteemployee);
    }
}
